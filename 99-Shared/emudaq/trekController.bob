<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-10-28 12:07:15 by yanns-->
<display version="2.0.0">
  <name>TREK AMP Controller</name>
  <width>340</width>
  <height>190</height>
  <widget type="rectangle" version="2.0.0">
    <name>BGGrey01-titlebar_1</name>
    <width>340</width>
    <height>190</height>
    <line_width>2</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>HEADER2</class>
    <text>$(SYS) Controller</text>
    <width>340</width>
    <height>46</height>
    <font use_class="true">
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <actions>
    </actions>
    <border_color>
      <color red="0" green="128" blue="255">
      </color>
    </border_color>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>BGGrey01-background_1</name>
    <x>16</x>
    <y>46</y>
    <width>310</width>
    <height>134</height>
    <line_width>2</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <corner_width>5</corner_width>
    <corner_height>5</corner_height>
  </widget>
  <widget type="slide_button" version="2.0.0">
    <name>Slide Button</name>
    <pv_name>$(Volt-On)</pv_name>
    <label></label>
    <x>140</x>
    <y>142</y>
    <width>50</width>
    <rules>
      <rule name="disabled" prop_id="enabled" out_exp="false">
        <exp bool_exp="pv0">
          <value>true</value>
        </exp>
        <pv_name>$(IOC)Vacuum-Ilock-Sts</pv_name>
      </rule>
    </rules>
    <enabled>false</enabled>
  </widget>
  <widget type="led" version="2.0.0">
    <name>LED</name>
    <pv_name>$(Trip)</pv_name>
    <x>198</x>
    <y>143</y>
    <width>112</width>
    <height>25</height>
    <off_label>Trip NOK</off_label>
    <off_color>
      <color name="MAJOR" red="252" green="13" blue="27">
      </color>
    </off_color>
    <on_label>TRIP OK</on_label>
    <on_color>
      <color name="ON" red="70" green="255" blue="70">
      </color>
    </on_color>
    <square>true</square>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_7</name>
    <text>HV Enabled:</text>
    <x>49</x>
    <y>144</y>
    <width>88</width>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>vacuum-error-on_1</name>
    <x>134</x>
    <y>142</y>
    <width>50</width>
    <height>27</height>
    <visible>false</visible>
    <line_width>4</line_width>
    <line_color>
      <color name="ERROR" red="252" green="13" blue="27">
      </color>
    </line_color>
    <background_color>
      <color red="204" green="204" blue="204">
      </color>
    </background_color>
    <transparent>true</transparent>
    <rules>
      <rule name="blink" prop_id="visible" out_exp="false">
        <exp bool_exp="!pv0 &amp;&amp; pv1">
          <value>true</value>
        </exp>
        <pv_name>$(IOC)Vacuum-Ilock-Sts</pv_name>
        <pv_name>sim://ramp(0,1,0.5)</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Voltage</name>
    <pv_name>$(Voltage)</pv_name>
    <x>238</x>
    <y>62</y>
    <width>72</width>
    <height>30</height>
    <precision>3</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="textentry" version="3.0.0">
    <name>Volt-SP</name>
    <pv_name>$(Volt-SP)</pv_name>
    <x>154</x>
    <y>62</y>
    <width>75</width>
    <height>30</height>
    <format>1</format>
    <precision>0</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <rules>
      <rule name="disabled-sp" prop_id="enabled" out_exp="false">
        <exp bool_exp="pv0">
          <value>true</value>
        </exp>
        <pv_name>$(IOC)Vacuum-Ilock-Sts</pv_name>
      </rule>
    </rules>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <enabled>false</enabled>
    <border_width>1</border_width>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_8</name>
    <text>Voltage SP/Mon:</text>
    <x>36</x>
    <y>64</y>
    <width>113</width>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_9</name>
    <text>Current Mon:</text>
    <x>36</x>
    <y>104</y>
    <width>113</width>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>vacuum-error-sp</name>
    <x>153</x>
    <y>62</y>
    <width>130</width>
    <height>30</height>
    <visible>false</visible>
    <line_width>4</line_width>
    <line_color>
      <color name="ERROR" red="252" green="13" blue="27">
      </color>
    </line_color>
    <background_color>
      <color red="204" green="204" blue="204">
      </color>
    </background_color>
    <transparent>true</transparent>
    <rules>
      <rule name="blink" prop_id="visible" out_exp="false">
        <exp bool_exp="!pv0 &amp;&amp; pv1">
          <value>true</value>
        </exp>
        <pv_name>$(IOC)Vacuum-Ilock-Sts</pv_name>
        <pv_name>sim://ramp(0,1,0.5)</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Current MOn</name>
    <pv_name>$(Current)</pv_name>
    <x>154</x>
    <y>101</y>
    <width>156</width>
    <height>30</height>
    <precision>3</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>vacuum-error-sp_1</name>
    <x>153</x>
    <y>101</y>
    <width>130</width>
    <height>30</height>
    <visible>false</visible>
    <line_width>4</line_width>
    <line_color>
      <color name="ERROR" red="252" green="13" blue="27">
      </color>
    </line_color>
    <background_color>
      <color red="204" green="204" blue="204">
      </color>
    </background_color>
    <transparent>true</transparent>
    <rules>
      <rule name="blink" prop_id="visible" out_exp="false">
        <exp bool_exp="!pv0 &amp;&amp; pv1">
          <value>true</value>
        </exp>
        <pv_name>$(IOC)Vacuum-Ilock-Sts</pv_name>
        <pv_name>sim://ramp(0,1,0.5)</pv_name>
      </rule>
    </rules>
  </widget>
</display>
