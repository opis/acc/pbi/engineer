from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

pv_mcp_check = PVUtil.getLong(pvs[0])

if pv_mcp_check > 0:
    ScriptUtil.showMessageDialog(widget, "You cannot enforce MCP setting mode if:\n"
                                         "HV_Cage_target < DELTA_MCP + 3000\n\n"\
                                         "Please switch back to HV setting mode, and enforce it with:\n"\
                                         "HV_Cage_target >= DELTA_MCP + 3000\n")
