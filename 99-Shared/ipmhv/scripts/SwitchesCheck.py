from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

switches_check = PVUtil.getLong(pvs[0])

if switches_check > 0:
    ScriptUtil.showMessageDialog(widget, "You cannot enforce your input if one of the following channels if off:\n"
                                        "- Positive HV cage\n"\
                                        "- Negative HV cage\n"\
                                        "- MCP in\n"\
                                        "- MCP out\n"\
                                        "- MCP screen\n\n\n"\
                                        "Make sure all those channels are on then try again.")
